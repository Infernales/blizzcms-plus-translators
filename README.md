# _BlizzCMS Plus Translators_
Repository for translation and multi-language integration in BlizzCMS.

[![Project Status](https://img.shields.io/badge/Status-In_Development-yellow.svg?style=flat-square)](#)
[![Translators Version](https://img.shields.io/badge/Version-0.0.2-green.svg?style=flat-square)](#)

# _Supported language 1.0.5_

| Translate | Percentage | Contributors | Status 
| :----------- | :---------- | :---------- | :----------
| **EN** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | WoW CMS Team | Updated
| **ES** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | WoW CMS Team | Updated
| **NO** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | [Mr.Angel](https://gitlab.com/vortexed.profesional) | Updated

# _Supported languages ​​outdated_


| Translate | Percentage | Contributors | Status 
| :----------- | :---------- | :---------- | :----------
| **DE** | [![Translate %](https://img.shields.io/badge/percentage-80%25-red.svg)](#) |  ilias2143#5804 and [Mordoth]() | Outdated
| **RU** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | [Infernales](https://gitlab.com/Infernales) | Updated
| **PTbr** | [![Translate %](https://img.shields.io/badge/percentage-80%25-red.svg)](#) | [XáXá](https://gitlab.com/gbmv33) | Outdated

# _Untranslated languages_


| Translate | Percentage | Contributors | Status 
| :----------- | :---------- | :---------- | :----------
| **FR** | [![Translate %](https://img.shields.io/badge/percentage-0%25-blueviolet.svg)](#) |  |
| **BG** | [![Translate %](https://img.shields.io/badge/percentage-0%25-blueviolet.svg)](#) |  |

## Copyright

Copyright © 2019 [WoW-CMS](https://wow-cms.com).
