<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang['admin_nav_dashboard'] = 'Dashboard';
$lang['admin_nav_settings'] = 'Einstellungen';
$lang['admin_nav_website_settings'] = 'Website Einstellungen';
$lang['admin_nav_manage_modules'] = 'Module bearbeiten';
$lang['admin_nav_manage_realms'] = 'Realms bearbeiten';
$lang['admin_nav_manage_slides'] = 'Slider bearbeiten';
$lang['admin_nav_users'] = 'Nutzer';
$lang['admin_nav_users_list'] = 'Nutzerliste';
$lang['admin_nav_chars_list'] = 'Charakterliste';
$lang['admin_nav_website'] = 'Website';
$lang['admin_nav_news'] = 'Neuigkeiten';
$lang['admin_nav_changelogs'] = 'Änderungsprotokoll';
$lang['admin_nav_pages'] = 'Seiten';
$lang['admin_nav_faq'] = 'FAQ';
$lang['admin_nav_donations'] = 'Spenden';
$lang['admin_nav_topsites'] = 'Topseiten';
$lang['admin_nav_store'] = 'Shop';
$lang['admin_nav_manage_groups'] = 'Gruppen bearbeiten';
$lang['admin_nav_manage_items'] = 'Items bearbeiten';
$lang['admin_nav_forum'] = 'Forum';
$lang['admin_nav_manage_categories'] = 'Kategorien bearbeiten';
$lang['admin_nav_manege_forums'] = 'Foren bearbeiten';

/*Button Lang*/
$lang['button_unban'] = 'Bann entfernen';
$lang['button_ban'] = 'Bannen';
$lang['button_re_grant_account'] = 'Rang entfernen';
$lang['button_grant_account'] = 'Rang gewähren';
$lang['button_change_level'] = 'Level ändern';
$lang['button_disable'] = 'Deaktivieren';
$lang['button_enable'] = 'Aktivieren';

/*Card Title Lang*/
$lang['card_title_user_manage'] = 'Nutzer bearbeiten';
$lang['card_title_unban_account'] = 'Account entbannen';
$lang['card_title_ban_account'] = 'Account bannen';
$lang['card_title_rank_account'] = 'Rang des Accounts';
$lang['card_title_general_info'] = 'Allgemeine Infos';
$lang['card_title_donate_history'] = 'Spendenverlauf';
$lang['card_title_annotations'] = 'Anmerkungen';
$lang['card_title_char_manage'] = 'Charkter bearbeiten';
$lang['card_title_change_level'] = 'Level ändern';
$lang['card_title_rename'] = 'Charakter umbenennen';
$lang['card_title_unban_char'] = 'Charakter entsperren';
$lang['card_title_ban_char'] = 'Charakter sperren';
$lang['card_title_customize'] = 'Charakter anpassen';
$lang['card_title_change_race'] = 'Rasse ändern';
$lang['card_title_change_faction'] = 'Fraktion ändern';
$lang['card_title_changelogs_list'] = 'Ãnderungsprotokolle';
$lang['card_title_pages_list'] = 'Auflistung der Seiten';
$lang['card_title_news_list'] = 'Neuigkeitenliste';
$lang['card_title_edit_news'] = 'Neuigkeiten bearbeiten';
$lang['card_title_edit_pages'] = 'Seiten bearbeiten';
$lang['card_title_edit_changelogs'] = 'Änderungsprotokolle bearbeiten';
$lang['card_title_edit_item'] = 'Gegenstände bearbeiten';
$lang['card_title_edit_group'] = 'Gruppen bearbeiten';
$lang['card_title_faq_list'] = 'FAQ Liste';
$lang['card_title_edit_topsite'] = 'Topseiten bearbeiten';
$lang['card_title_edit_forum'] = 'Foren bearbeiten';

/*Table header Lang*/
$lang['table_header_race'] = 'Rasse';
$lang['table_header_class'] = 'Klasse';
$lang['table_header_level'] = 'Level';
$lang['table_header_money'] = 'Vermögen';
$lang['table_header_own'] = 'Nutzer';
$lang['table_header_action'] = 'Aktion';
$lang['table_header_realm_id'] = 'Realm ID';
$lang['table_header_realm_name'] = 'Realm Name';
$lang['table_header_realm_char_database'] = 'Charakter Datenbank';
$lang['table_header_tax'] = 'Steuer';
$lang['table_header_points'] = 'Punkte';
$lang['table_header_type'] = 'Type';
$lang['table_header_module'] = 'Module';

/*Input Placeholder Lang*/
$lang['placeholder_create_changelog'] = 'Neues Änderungsprotokoll erstellen';
$lang['placeholder_changelog_title'] = 'Überschrift';
$lang['placeholder_create_pages'] = 'Seite erstellen';
$lang['placeholder_create_news'] = 'Neuigkeit erstellen';
$lang['placeholder_create_donation'] = 'Spenden';
$lang['placeholder_donation_title'] = 'Überschrift der Spende';
$lang['placeholder_news_title'] = 'Üerschrift der Neuigkeit';
$lang['placeholder_upload_file'] = 'Datei hochladen';
$lang['placeholder_create_category'] = 'Kategorie erstellen';
$lang['placeholder_create_forums'] = 'Forum erstellen';
$lang['placeholder_category_title'] = 'Überschrift der Kategorie';
$lang['placeholder_forum_title'] = 'Überschrift des Forums';
$lang['placeholder_forum_description'] = 'Forenbeschreibung';
$lang['placeholder_forum_icon_name'] = 'Icon Name';
$lang['placeholder_category'] = 'Kategorie';
$lang['placeholder_create_item'] = 'Gegenstand erstellen';
$lang['placeholder_store_item_name'] = 'Gegenstandsname';
$lang['placeholder_store_item_id'] = 'Gegenstands Id';
$lang['placeholder_store_image_name'] = 'Bilddateiname';
$lang['placeholder_create_group'] = 'Gruppe erstellen';
$lang['placeholder_group_title'] = 'Überschrift der Gruppe';
$lang['placeholder_create_faq'] = 'FAQ erstellen';
$lang['placeholder_faq_title'] = 'Überschrift vom Faq';
$lang['placeholder_create_topsite'] = 'Topseite erstellen';
$lang['placeholder_reason'] = 'Grund';
$lang['placeholder_gmlevel'] = 'Account Level';
$lang['placeholder_forum_icon'] = 'Ordner/Bild.jpg oder Ordner/Bild.png';

/*Status Lang*/
$lang['status_is_online'] = 'Spieler ist Online, bitte abmelden';
$lang['status_name_exist'] = 'Dieser Name ist schon vergeben';
$lang['status_donate_complete'] = 'Abgeschlossen';
$lang['status_donate_cancell'] = 'Abgebrochen';

/*Options Lang*/
$lang['option_yes'] = 'Ja';
$lang['option_no'] = 'Nein';
$lang['option_everyone'] = 'Jeder';
$lang['option_staff'] = 'Team';
$lang['option_all'] = 'Team - Jeder';
$lang['option_item'] = 'Gegenstand';

/*Count Lang*/
$lang['count_accounts_created'] = 'Erstellte Accounts';
$lang['count_accounts_banned'] = 'Gesperrte Accounts';
$lang['count_news_created'] = 'Erstellte Neuigkeiten';
$lang['count_changelogs_created'] = 'Erstellte Änderungsprotokolle';
$lang['total_accounts_registered'] = 'Registrierte Accounts.';
$lang['total_accounts_banned'] = 'Gesperrte Accounts.';
$lang['total_news_writed'] = 'Geschriebene Neuigkeiten.';
$lang['total_changelogs_writed'] = 'Geschriebene Änderungsprotokolle.';

/*Alert Lang*/
$lang['alert_new_page_url'] = 'Dein neuer Seitenurl ist';
$lang['alert_banned_reason'] = 'ist gesperrt, Grund:';

/*Logs Lang*/
$lang['log_new_level'] = 'Neues Level erhalten';
$lang['log_old_level'] = 'Vorher war es';
$lang['log_new_name'] = 'Es hat ein neuen Namen';
$lang['log_old_name'] = 'Vorher war es';
$lang['log_unbanned'] = 'Entsperrt';
$lang['log_customization'] = 'eine Anpassung erhalten';
$lang['log_change_race'] = 'Rassenänderung erhalten';
$lang['log_change_faction'] = 'Fraktionsänderung erhalten';
$lang['log_banned'] = 'wurde gesperrt';
$lang['log_gm_assigned'] = 'Rang erhalten';
$lang['log_gm_removed'] = 'Rang wurde entfernt';
