<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang['admin_nav_dashboard'] = 'Dashbord';
$lang['admin_nav_system'] = 'System';
$lang['admin_nav_manage_settings'] = 'Innstillinger';
$lang['admin_nav_manage_modules'] = 'Administrer Moduler';
$lang['admin_nav_users'] = 'Brukere';
$lang['admin_nav_accounts'] = 'Kontoer';
$lang['admin_nav_website'] = 'Hjemmeside';
$lang['admin_nav_menu'] = 'Meny';
$lang['admin_nav_realms'] = 'Realms';
$lang['admin_nav_slides'] = 'Lysbilder';
$lang['admin_nav_news'] = 'Nyheter';
$lang['admin_nav_changelogs'] = 'Forandringer';
$lang['admin_nav_pages'] = 'Sider';
$lang['admin_nav_donate_methods'] = 'Donerings Metoder';
$lang['admin_nav_topsites'] = 'Topp Sider';
$lang['admin_nav_donate_vote_logs'] = 'Donering/Stemme Logger';
$lang['admin_nav_store'] = 'Butikk';
$lang['admin_nav_manage_store'] = 'Administrer Butikk';
$lang['admin_nav_forum'] = 'Forum';
$lang['admin_nav_manage_forum'] = 'Administrer Forum';
$lang['admin_nav_logs'] = 'Logg System';

/*Sections Lang*/
$lang['section_general_settings'] = 'Generelle Innstillinger';
$lang['section_module_settings'] = 'Modul Innstillinger';
$lang['section_optional_settings'] = 'Valgfrie Innstillinger';
$lang['section_seo_settings'] = 'SEO Innstillinger';
$lang['section_update_cms'] = 'Oppdater CMS';
$lang['section_check_information'] = 'Sjekk Informasjon';
$lang['section_forum_categories'] = 'Forum Kategorier';
$lang['section_forum_elements'] = 'Forum Elementer';
$lang['section_store_categories'] = 'Butikk Kategorier';
$lang['section_store_items'] = 'Butikk Utstyr';
$lang['section_store_top'] = 'Butikk TOP Utstyr';
$lang['section_logs_dp'] = 'Donerings Logg';
$lang['section_logs_vp'] = 'Stemme Logg';

/*Button Lang*/
$lang['button_select'] = 'Valg';
$lang['button_update'] = 'Oppdater';
$lang['button_unban'] = 'Tilgi';
$lang['button_ban'] = 'Forby';
$lang['button_remove'] = 'Slett';
$lang['button_grant'] = 'Gi';
$lang['button_update_version'] = 'Oppdater Til Siste Versjon';

/*Table header Lang*/
$lang['table_header_race'] = 'Rase';
$lang['table_header_class'] = 'Klasse';
$lang['table_header_level'] = 'Nivå';
$lang['table_header_money'] = 'Penger';
$lang['table_header_time_played'] = 'Tid Spilt';
$lang['table_header_actions'] = 'Handlinger';
$lang['table_header_id'] = '#ID';
$lang['table_header_tax'] = 'Skatt';
$lang['table_header_points'] = 'Poeng';
$lang['table_header_type'] = 'Type';
$lang['table_header_module'] = 'Modul';
$lang['table_header_payment_id'] = 'Kjøpe ID';
$lang['table_header_hash'] = 'Hash';
$lang['table_header_total'] = 'Total';
$lang['table_header_create_time'] = 'Tid Laget';
$lang['table_header_guid'] = 'Guid';
$lang['table_header_information'] = 'Informasjon';
$lang['table_header_value'] = 'Verdi';

/*Input Placeholder Lang*/
$lang['placeholder_manage_account'] = 'Administrer Konto';
$lang['placeholder_update_information'] = 'Oppdater Konto Informasjon';
$lang['placeholder_donation_logs'] = 'Donerings Logg';
$lang['placeholder_store_logs'] = 'Butikk Logg';
$lang['placeholder_create_changelog'] = 'Lag Endringslogg';
$lang['placeholder_edit_changelog'] = 'Endre Endringslogg';
$lang['placeholder_create_category'] = 'Lag Kategori';
$lang['placeholder_edit_category'] = 'Endre Kategori';
$lang['placeholder_create_forum'] = 'Lag Forum';
$lang['placeholder_edit_forum'] = 'Endre Forum';
$lang['placeholder_create_menu'] = 'Lag Ment';
$lang['placeholder_edit_menu'] = 'Endre Ment';
$lang['placeholder_create_news'] = 'Lag Nyheter';
$lang['placeholder_edit_news'] = 'Endre Nyheter';
$lang['placeholder_create_page'] = 'Lag Side';
$lang['placeholder_edit_page'] = 'Endre Side';
$lang['placeholder_create_realm'] = 'Lag Realm';
$lang['placeholder_edit_realm'] = 'Endre Realm';
$lang['placeholder_create_slide'] = 'Lag Lysbilde';
$lang['placeholder_edit_slide'] = 'Endre Lysbilde';
$lang['placeholder_create_item'] = 'Lag Utstyr';
$lang['placeholder_edit_item'] = 'Endre Utstyr';
$lang['placeholder_create_topsite'] = 'Lag Topp Side';
$lang['placeholder_edit_topsite'] = 'Endre Topp Side';
$lang['placeholder_create_top'] = 'Lag TOP Utstyr';
$lang['placeholder_edit_top'] = 'Endre TOP Utstyr';

$lang['placeholder_upload_image'] = 'Last Opp Bilde';
$lang['placeholder_icon_name'] = 'Icon Navn';
$lang['placeholder_category'] = 'Kategori';
$lang['placeholder_name'] = 'Navn';
$lang['placeholder_item'] = 'Utstyr';
$lang['placeholder_image_name'] = 'Bilde Navn';
$lang['placeholder_reason'] = 'Begrunnelse';
$lang['placeholder_gmlevel'] = 'GM Nivå';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_child_menu'] = 'Undermeny';
$lang['placeholder_url_type'] = 'URL Type';
$lang['placeholder_route'] = 'Rute';
$lang['placeholder_hours'] = 'Timer';
$lang['placeholder_soap_hostname'] = 'SOAP Vertsnavn';
$lang['placeholder_soap_port'] = 'SOAP Port';
$lang['placeholder_soap_user'] = 'SOAP Bruker';
$lang['placeholder_soap_password'] = 'SOAP Passord';
$lang['placeholder_db_character'] = 'Karakter';
$lang['placeholder_db_hostname'] = 'Database Vertsnavn';
$lang['placeholder_db_name'] = 'Database Navn';
$lang['placeholder_db_user'] = 'Database Bruker';
$lang['placeholder_db_password'] = 'Database Passord';
$lang['placeholder_account_points'] = 'Konto Poeng';
$lang['placeholder_account_ban'] = 'Forby Konto';
$lang['placeholder_account_unban'] = 'Tilgi Konto';
$lang['placeholder_account_grant_rank'] = 'Gi GM Nivå';
$lang['placeholder_account_remove_rank'] = 'Fjern GM Nivå';
$lang['placeholder_command'] = 'Kommando';

/*Config Lang*/
$lang['conf_website_name'] = 'Hjemmeside Navn';
$lang['conf_realmlist'] = 'Realmlist';
$lang['conf_discord_invid'] = 'Discord Invitasjons ID';
$lang['conf_timezone'] = 'Tidssone';
$lang['conf_theme_name'] = 'Tema Navn';
$lang['conf_maintenance_mode'] = 'Vedlikeholdsmodus';
$lang['conf_social_facebook'] = 'Facebook URL';
$lang['conf_social_twitter'] = 'Twitter URL';
$lang['conf_social_youtube'] = 'YouTube URL';
$lang['conf_paypal_currency'] = 'PayPal Valuta';
$lang['conf_paypal_mode'] = 'PayPal Modus';
$lang['conf_paypal_client'] = 'PayPal Klient ID';
$lang['conf_paypal_secretpass'] = 'PayPal Hemmelig Password';
$lang['conf_default_description'] = 'Standard Forklaring';
$lang['conf_admin_gmlvl'] = 'Administrator GM Nivå';
$lang['conf_mod_gmlvl'] = 'Moderator GM Nivå';
$lang['conf_recaptcha_key'] = 'reCaptcha Site Key';
$lang['conf_account_activation'] = 'Kontoaktivering';
$lang['conf_smtp_hostname'] = 'SMTP Vertsnavn';
$lang['conf_smtp_port'] = 'SMTP Port';
$lang['conf_smtp_encryption'] = 'SMTP Kryptering';
$lang['conf_smtp_username'] = 'SMTP Bruker';
$lang['conf_smtp_password'] = 'SMTP Passord';
$lang['conf_sender_email'] = 'Sender Epost';
$lang['conf_sender_name'] = 'Sender Navn';

/*Logs */
$lang['placeholder_logs_dp'] = 'Donasjon';
$lang['placeholder_logs_quantity'] = 'Mengde';
$lang['placeholder_logs_hash'] = 'Hash';
$lang['placeholder_logs_voteid'] = 'Stemme ID';
$lang['placeholder_logs_points'] = 'Poeng';
$lang['placeholder_logs_lasttime'] = 'Sist';
$lang['placeholder_logs_expiredtime'] = 'Utløpt tid';

/*Status Lang*/
$lang['status_completed'] = 'Fullført';
$lang['status_cancelled'] = 'Avbrutt';

/*Options Lang*/
$lang['option_normal'] = 'Normal';
$lang['option_dropdown'] = 'Nedtrekk';
$lang['option_image'] = 'Bilde';
$lang['option_video'] = 'Video';
$lang['option_iframe'] = 'Iframe';
$lang['option_enabled'] = 'Aktivert';
$lang['option_disabled'] = 'Deaktivert';
$lang['option_ssl'] = 'SSL';
$lang['option_tls'] = 'TLS';
$lang['option_everyone'] = 'Alle';
$lang['option_staff'] = 'PERSONALE';
$lang['option_all'] = 'PERSONALE - Alle';
$lang['option_rename'] = 'Gi nytt navn';
$lang['option_customize'] = 'Tilpass';
$lang['option_change_faction'] = 'Endre fraksjon';
$lang['option_change_race'] = 'Endre Rase';
$lang['option_dp'] = 'DP';
$lang['option_vp'] = 'VP';
$lang['option_dp_vp'] = 'DP & VP';
$lang['option_internal_url'] = 'Internal URL';
$lang['option_external_url'] = 'External URL';
$lang['option_on'] = 'På';
$lang['option_off'] = 'Av';

/*Count Lang*/
$lang['count_accounts_created'] = 'Opprettet Kontoer';
$lang['count_accounts_banned'] = 'Bannlyste Kontoer';
$lang['count_news_created'] = 'Nyheter Laget';
$lang['count_changelogs_created'] = 'Forandringslogg Laget';
$lang['total_accounts_registered'] = 'Totalt Registrerte Kontoer.';
$lang['total_accounts_banned'] = 'Totalt Bannlyste Kontoer.';
$lang['total_news_writed'] = 'Totalt Nyheter Skrevet.';
$lang['total_changelogs_writed'] = 'Totalt Forandringslogg Skrevet.';

$lang['info_alliance_players'] = 'Allianse Spillere';
$lang['info_alliance_playing'] = 'Totalt Allianser På Denne Realm';
$lang['info_horde_players'] = 'Horde Spillere';
$lang['info_horde_playing'] = 'Totalt Horder På Denne Realm';
$lang['info_players_playing'] = 'Spillere På Denne Realm';

/*Alert Lang*/
$lang['alert_smtp_activation'] = 'Viss du aktiverer dette alternativet må du konfigurere SMTP for og sende eposter.';
$lang['alert_banned_reason'] = 'Er Bannlyst, Grunn:';

/*Logs Lang*/
$lang['log_new_level'] = 'Motta et nytt nivå';
$lang['log_old_level'] = 'Før var det';
$lang['log_new_name'] = 'Det har et nytt navn';
$lang['log_old_name'] = 'Før var det';
$lang['log_unbanned'] = 'Tilgitt';
$lang['log_customization'] = 'Få en tilpasning';
$lang['log_change_race'] = 'Få en rase forandring';
$lang['log_change_faction'] = 'Få en fraksjonsendring';
$lang['log_banned'] = 'Var bannlyst';
$lang['log_gm_assigned'] = 'Mottok GM nivå';
$lang['log_gm_removed'] = 'GM nivå ble fjernet';

/*CMS Lang*/
$lang['cms_version_currently'] = 'Denne versjonen kjører for øyeblikket';
$lang['cms_warning_update'] = 'Når cms-en blir oppdatert, kan konfigurasjonen gjenopprettes til standard, avhengig av endringene som er gjort i hver versjon.';
$lang['cms_php_version'] = 'PHP Versjon';
$lang['cms_allow_fopen'] = 'allow_url_fopen';
$lang['cms_allow_include'] = 'allow_url_include';
$lang['cms_loaded_modules'] = 'Lastede Moduler';
$lang['cms_loaded_extensions'] = 'Lastede Utvidelser';
