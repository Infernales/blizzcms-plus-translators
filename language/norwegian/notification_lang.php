<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Notification Title Lang*/
$lang['notification_title_success'] = 'Suksess';
$lang['notification_title_warning'] = 'Advarsel';
$lang['notification_title_error'] = 'Error';
$lang['notification_title_info'] = 'Informasjon';

/*Notification Message (Login/Register) Lang*/
$lang['notification_username_empty'] = 'Brukernavn er tomt';
$lang['notification_email_empty'] = 'E-postadressen er tom';
$lang['notification_password_empty'] = 'Passordet er tomt';
$lang['notice_user_error'] = 'Brukernavnet eller passordet er feil. Vær så snill, prøv på nytt!';
$lang['varsel_email_error '] =' E-postadressen eller passordet er feil. Vær så snill, prøv på nytt!';
$lang['notification_check_email'] = 'Brukernavnet eller e-postadressen er feil. Vær så snill, prøv på nytt!';
$lang['notice_checking'] = 'Kontrollerer ...';
$lang['notice_redirection'] = 'Koble til kontoen din ...';
$lang['notice_new_account'] = 'Ny konto opprettet. omdirigerer til innlogging ... ';
$lang['varsel_email_sent '] =' E-post sendt. Vennligst sjekk din e-post...';
$lang['notice_account_activation'] = 'E-post sendt. Vennligst sjekk e-posten din for å aktivere kontoen din. ';
$lang['notice_captcha_error'] = 'Kontroller captcha';
$lang['varsel_passord_lenght_error '] =' Feil passordlengde. bruk et passord mellom 5 og 16 tegn ';
$lang['notice_account_already_exist'] = 'Denne kontoen eksisterer allerede';
$lang['notice_password_not_match'] = 'Passord stemmer ikke overens';
$lang['notice_same_password'] = 'Passordet er det samme.';
$lang['notice_currentpass_not_match'] = 'Det gamle passordet stemmer ikke';
$lang['notice_used_email'] = 'E-post i bruk';
$lang['notice_email_not_match'] = 'E-post stemmer ikke';
$lang['notice_expansion_not_found'] = 'Utvidelse ikke funnet';
$lang['varsel_valid_key '] =' Konto aktivert ';
$lang['varsel_valid_key_desc '] =' Nå kan du logge på med kontoen din. ';
$lang['notice_invalid_key'] = 'Aktiveringsnøkkelen som følger med er ikke gyldig.';

/*Notification Message (General) Lang*/
$lang['notice_email_chang'] = 'E-postadressen er endret.';
$lang['notice_password_chang'] = 'Passordet er endret.';
$lang['notice_avatar_chang'] = 'Avataren er endret.';
$lang['varsel_wrong_values ​​'] =' Verdiene er gale ';
$lang['varsel_select_type '] =' Velg en type ';
$lang['notice_select_priority'] = 'Velg en prioritet';
$lang['notice_select_category'] = 'Velg en kategori';
$lang['varsel_select_realm '] =' Velg en Realm ';
$lang['notice_select_character'] = 'Velg en Karakter';
$lang['varsel_select_item '] =' Velg et Utstyr ';
$lang['notice_report_created'] = 'Rapporten er opprettet.';
$lang['varsel_title_empty '] =' Tittelen er tom ';
$lang['notice_description_empty'] = 'Beskrivelse er tom';
$lang['notice_name_empty'] = 'Navnet er tomt';
$lang['notice_id_empty'] = 'ID er tom';
$lang['notice_reply_empty'] = 'Svaret er tomt';
$lang['notice_reply_created'] = 'Svar har blitt sendt.';
$lang['notice_reply_deleted'] = 'Svaret er slettet.';
$lang['notice_topic_created'] = 'Emnet er opprettet.';
$lang['notification_donation_successful'] = 'Donasjonen er fullført, sjekk donor poengene dine på kontoen din.';
$lang['notice_donation_canceled'] = 'Donasjonen er kansellert.';
$lang['notice_donation_error'] = 'Informasjonen som er gitt i transaksjonen stemmer ikke overens.';
$lang['varsel_store_chars_error '] =' Velg karakteren din i hvert element. ';
$lang['varsel_store_item_insufficient_points '] =' Du har ikke nok poeng å kjøpe. ';
$lang['varsel_store_item_purchased '] =' Varene er kjøpt, sjekk posten din i spillet. ';
$lang['notice_store_item_added'] = 'Det valgte elementet er lagt til i handlekurven din.';
$lang['varsel_store_item_removed '] =' Det valgte elementet er fjernet fra handlekurven. ';
$lang['varsel_store_cart_error '] =' Handlevognoppdateringen mislyktes, prøv igjen. ';

/*Notification Message (Admin) Lang*/
$lang['varsel_changelog_created '] =' Forandringsloggen er opprettet. ';
$lang['varsel_changelog_edited '] =' Endringsloggen er redigert. ';
$lang['notification_changelog_deleted'] = 'Endringsloggen er slettet.';
$lang['notice_forum_created'] = 'Forumet er opprettet.';
$lang['notice_forum_edited'] = 'Forumet er redigert.';
$lang['notice_forum_deleted'] = 'Forumet er slettet.';
$lang['notice_category_created'] = 'Kategorien er opprettet.';
$lang['notice_category_edited'] = 'Kategorien er redigert.';
$lang['notice_category_deleted'] = 'Kategorien er slettet.';
$lang['notice_menu_created'] = 'Menyen er opprettet.';
$lang['notice_menu_edited'] = 'Menyen er redigert.';
$lang['notice_menu_deleted'] = 'Menyen er slettet.';
$lang['notice_news_deleted'] = 'Nyhetene er slettet.';
$lang['notice_page_created'] = 'Siden er opprettet.';
$lang['notice_page_edited'] = 'Siden er redigert.';
$lang['notice_page_deleted'] = 'Siden er slettet.';
$lang['notice_realm_created'] = 'Realm er opprettet.';
$lang['notice_realm_edited'] = 'Realm er redigert.';
$lang['notice_realm_deleted'] = 'Realm er slettet.';
$lang['notice_slide_created'] = 'Lysbildet er opprettet.';
$lang['notice_slide_edited'] = 'Lysbildet er redigert.';
$lang['notice_slide_deleted'] = 'Lysbildet er slettet.';
$lang['varsel_item_created '] =' Elementet er opprettet. ';
$lang['notice_item_edited'] = 'Elementet er redigert.';
$lang['notice_item_deleted'] = 'Elementet er slettet.';
$lang['notice_top_created'] = 'TOP er opprettet.';
$lang['notice_top_edited'] = 'TOP er redigert.';
$lang['notice_top_deleted'] = 'TOP er slettet.';
$lang['notice_topsite_created'] = 'Toppsidene er opprettet.';
$lang['varsel_topsite_edited '] =' Toppsidene er redigert. ';
$lang['notice_topsite_deleted'] = 'Toppsidene er slettet.';

$lang['varsel_settings_updated '] =' Innstillingene er oppdatert. ';
$lang['notification_module_enabled'] = 'Modulen er aktivert.';
$lang['notice_module_disabled'] = 'Modulen er deaktivert.';
$lang['varsel_migrasjon '] =' Innstillingene er satt. ';

$lang['notice_donation_added'] = 'Lagt til donasjon';
$lang['notice_donation_deleted'] = 'Slettet donasjon';
$lang['notice_donation_updated'] = 'Oppdatert donasjon';
$lang['notice_points_empty'] = 'Poengene er tomme';
$lang['varsel_tax_empty '] =' Skatt er tom ';
$lang['notice_price_empty'] = 'Prisen er tom';
$lang['notice_incorrect_update'] = 'Uventet oppdatering';

$lang['varsel_route_inuse '] =' Ruten er allerede i bruk, velg en annen. ';

$lang['notice_account_updated'] = 'Kontoen er oppdatert.';
$lang['varsel_dp_vp_empty '] =' DP / VP er tom ';
$lang['notice_account_banned'] = 'Kontoen er utestengt.';
$lang['notice_reason_empty'] = 'Årsaken er tom';
$lang['varsel_account_ban_remove '] =' Forbudet på kontoen er fjernet. ';
$lang['notice_rank_empty'] = 'Rangeringen er tom';
$lang['notice_rank_granted'] = 'Rangeringen er gitt.';
$lang['notice_rank_removed'] = 'Rangeringen er slettet.';

$lang['notice_cms_updated'] = 'CMS er oppdatert';
$lang['notice_cms_update_error'] = 'CMS kunne ikke oppdateres';
$lang['notice_cms_not_updated'] = 'Det er ikke funnet en ny versjon som skal oppdateres';
