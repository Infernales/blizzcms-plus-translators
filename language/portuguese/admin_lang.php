<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang['admin_nav_dashboard'] = 'Painel de controle';
$lang['admin_nav_system'] = 'Sistema';
$lang['admin_nav_manage_settings'] = 'Gerenciar configurações';
$lang['admin_nav_manage_modules'] = 'Gerenciar Módulos';
$lang['admin_nav_users'] = 'Usuários';
$lang['admin_nav_accounts'] = 'Contas';
$lang['admin_nav_website'] = 'Website';
$lang['admin_nav_menu'] = 'Menu';
$lang['admin_nav_realms'] = 'Reinos';
$lang['admin_nav_slides'] = 'Slides';
$lang['admin_nav_news'] = 'Notícias';
$lang['admin_nav_changelogs'] = 'Changelogs';
$lang['admin_nav_pages'] = 'Páginas';
$lang['admin_nav_donate_methods'] = 'Métodos de Doação';
$lang['admin_nav_topsites'] = 'Topsites';
$lang['admin_nav_donate_vote_logs'] = 'Donate/Vote Logs';
$lang['admin_nav_store'] = 'Loja';
$lang['admin_nav_manage_store'] = 'Gerenciar loja';
$lang['admin_nav_forum'] = 'Forum';
$lang['admin_nav_manage_forum'] = 'Gerenciar Fórum';

/*Sections Lang*/
$lang['section_general_settings'] = 'Configurações gerais';
$lang['section_module_settings'] = 'Configurações do módulo';
$lang['section_optional_settings'] = 'Configurações opcionais';
$lang['section_seo_settings'] = 'Configurações de SEO';
$lang['section_update_cms'] = 'Atualizar CMS';
$lang['section_check_information'] = 'Checkar Informações';
$lang['section_forum_categories'] = 'Categorias do Fórum';
$lang['section_forum_elements'] = 'Elementos do Fórum';
$lang['section_store_categories'] = 'Categorias de loja';
$lang['section_store_items'] = 'Itens da loja';
$lang['section_store_top'] = 'Loja TOP Items';

/*Button Lang*/
$lang['button_select'] = 'Selecionar';
$lang['button_update'] = 'Atualizar';
$lang['button_unban'] = 'Desbanir';
$lang['button_ban'] = 'Banir';
$lang['button_remove'] = 'Remover';
$lang['button_grant'] = 'Grant';
$lang['button_update_version'] = 'Atualização para a versão mais recente';

/*Table header Lang*/
$lang['table_header_race'] = 'Raça';
$lang['table_header_class'] = 'Classe';
$lang['table_header_level'] = 'Level';
$lang['table_header_money'] = 'Dinheiro';
$lang['table_header_time_played'] = 'Tempo jogado';
$lang['table_header_actions'] = 'Ações';
$lang['table_header_id'] = '#ID';
$lang['table_header_tax'] = 'Taxa';
$lang['table_header_points'] = 'Pontos';
$lang['table_header_type'] = 'Tipo';
$lang['table_header_module'] = 'Módulos';
$lang['table_header_payment_id'] = 'ID de pagamento';
$lang['table_header_hash'] = 'Hash';
$lang['table_header_total'] = 'Total';
$lang['table_header_create_time'] = 'Criar tempo';
$lang['table_header_guid'] = 'Guid';
$lang['table_header_information'] = 'Informações';
$lang['table_header_value'] = 'Valor';

/*Input Placeholder Lang*/
$lang['placeholder_manage_account'] = 'Gerenciar conta';
$lang['placeholder_update_information'] = 'Atualizar informações da conta';
$lang['placeholder_donation_logs'] = 'Logs de Doação';
$lang['placeholder_store_logs'] = 'Logs da loja';
$lang['placeholder_create_changelog'] = 'Criar Changelog';
$lang['placeholder_edit_changelog'] = 'Editar Changelog';
$lang['placeholder_create_category'] = 'Criar Categoria';
$lang['placeholder_edit_category'] = 'Editar Categoria';
$lang['placeholder_create_forum'] = 'Criar Fórum';
$lang['placeholder_edit_forum'] = 'Editar Fórum';
$lang['placeholder_create_menu'] = 'Criar Menu';
$lang['placeholder_edit_menu'] = 'Editar Menu';
$lang['placeholder_create_news'] = 'Criar Notícias';
$lang['placeholder_edit_news'] = 'Editar Notícias';
$lang['placeholder_create_page'] = 'Criar página';
$lang['placeholder_edit_page'] = 'Editar página';
$lang['placeholder_create_realm'] = 'Criar reino';
$lang['placeholder_edit_realm'] = 'Editar reino';
$lang['placeholder_create_slide'] = 'Criar Slide';
$lang['placeholder_edit_slide'] = 'Editar Slide';
$lang['placeholder_create_item'] = 'Criar item';
$lang['placeholder_edit_item'] = 'Editar item';
$lang['placeholder_create_topsite'] = 'Criar Topsite';
$lang['placeholder_edit_topsite'] = 'Editar Topsite';
$lang['placeholder_create_top'] = 'Criar TOP Item';
$lang['placeholder_edit_top'] = 'Editar TOP Item';

$lang['placeholder_upload_image'] = 'Carregar imagem';
$lang['placeholder_icon_name'] = 'Nome do Ícone';
$lang['placeholder_category'] = 'Categoria';
$lang['placeholder_name'] = 'Nome';
$lang['placeholder_item'] = 'Item';
$lang['placeholder_image_name'] = 'Nome da imagem';
$lang['placeholder_reason'] = 'Razão';
$lang['placeholder_gmlevel'] = 'GM Level';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_child_menu'] = 'Child Menu';
$lang['placeholder_url_type'] = 'Tipo de URL';
$lang['placeholder_route'] = 'Rota';
$lang['placeholder_hours'] = 'Horas';
$lang['placeholder_soap_hostname'] = 'Soap Hostname';
$lang['placeholder_soap_port'] = 'Soap Port';
$lang['placeholder_soap_user'] = 'Soap User';
$lang['placeholder_soap_password'] = 'Soap Password';
$lang['placeholder_db_character'] = 'Personagem';
$lang['placeholder_db_hostname'] = 'Database Hostname';
$lang['placeholder_db_name'] = 'Database Name';
$lang['placeholder_db_user'] = 'Database User';
$lang['placeholder_db_password'] = 'Database Password';
$lang['placeholder_account_points'] = 'Pontos de Conta';
$lang['placeholder_account_ban'] = 'Banir Conta';
$lang['placeholder_account_unban'] = 'Desbanir Conta';
$lang['placeholder_account_grant_rank'] = 'Dar GM Rank';
$lang['placeholder_account_remove_rank'] = 'Remover GM Rank';
$lang['placeholder_command'] = 'Comando';

/*Config Lang*/
$lang['conf_website_name'] = 'Nome do site';
$lang['conf_realmlist'] = 'Realmlist';
$lang['conf_discord_invid'] = 'ID do convite do discord';
$lang['conf_timezone'] = 'Fuso horário';
$lang['conf_theme_name'] = 'Nome do tema';
$lang['conf_maintenance_mode'] = 'Modo de manutenção';
$lang['conf_social_facebook'] = 'Facebook URL';
$lang['conf_social_twitter'] = 'Twitter URL';
$lang['conf_social_youtube'] = 'Youtube URL';
$lang['conf_paypal_currency'] = 'Moeda do PayPal';
$lang['conf_paypal_mode'] = 'Modo PayPal';
$lang['conf_paypal_client'] = 'PayPal Client ID';
$lang['conf_paypal_secretpass'] = 'PayPal Secret Password';
$lang['conf_default_description'] = 'Descrição Padrão';
$lang['conf_admin_gmlvl'] = 'Administrador GMLevel';
$lang['conf_mod_gmlvl'] = 'Moderador GMLevel';
$lang['conf_recaptcha_key'] = 'Chave do site reCaptcha';
$lang['conf_account_activation'] = 'Ativação de conta';
$lang['conf_smtp_hostname'] = 'SMTP Hostname';
$lang['conf_smtp_port'] = 'SMTP Port';
$lang['conf_smtp_encryption'] = 'SMTP Encryption';
$lang['conf_smtp_username'] = 'SMTP Username';
$lang['conf_smtp_password'] = 'SMTP Password';
$lang['conf_sender_email'] = 'E-mail do remetente';
$lang['conf_sender_name'] = 'Nome do remetente';

/*Status Lang*/
$lang['status_completed'] = 'Concluído';
$lang['status_cancelled'] = 'Cancelado';

/*Options Lang*/
$lang['option_normal'] = 'Normal';
$lang['option_dropdown'] = 'Dropdown';
$lang['option_image'] = 'Imagem';
$lang['option_video'] = 'Video';
$lang['option_iframe'] = 'Iframe';
$lang['option_enabled'] = 'Ativado';
$lang['option_disabled'] = 'Desativado';
$lang['option_ssl'] = 'SSL';
$lang['option_tls'] = 'TLS';
$lang['option_everyone'] = 'Todos';
$lang['option_staff'] = 'STAFF';
$lang['option_all'] = 'STAFF - Todos';
$lang['option_rename'] = 'Renomear';
$lang['option_customize'] = 'Customizar';
$lang['option_change_faction'] = 'Modificar Facção';
$lang['option_change_race'] = 'Mudar Raça';
$lang['option_dp'] = 'DP';
$lang['option_vp'] = 'VP';
$lang['option_dp_vp'] = 'DP & VP';
$lang['option_internal_url'] = 'URL interna';
$lang['option_external_url'] = 'URL externa';
$lang['option_on'] = 'On';
$lang['option_off'] = 'Off';

/*Count Lang*/
$lang['count_accounts_created'] = 'Contas criadas';
$lang['count_accounts_banned'] = 'Contas Banidas';
$lang['count_news_created'] = 'Notícias Criadas';
$lang['count_changelogs_created'] = 'Changelogs criados';
$lang['total_accounts_registered'] = 'Total de contas registadas.';
$lang['total_accounts_banned'] = 'Total de contas banidas.';
$lang['total_news_writed'] = 'Total de notícias escritas.';
$lang['total_changelogs_writed'] = 'Total de changelogs escritos.';

$lang['info_alliance_players'] = 'Jogadores da Aliança';
$lang['info_alliance_playing'] = 'Alianças jogando no reino';
$lang['info_horde_players'] = 'Jogadores de Horda';
$lang['info_horde_playing'] = 'Hordas jogando no reino';
$lang['info_players_playing'] = 'Jogadores jogando no reino';

/*Alert Lang*/
$lang['alert_smtp_activation'] = 'Se activar esta opção, tem de configurar o SMTP para enviar e-mails.';
$lang['alert_banned_reason'] = 'Está banido, razão:';

/*Logs Lang*/
$lang['log_new_level'] = 'Receba um novo nível';
$lang['log_old_level'] = 'Antes era';
$lang['log_new_name'] = 'Tem um novo nome';
$lang['log_old_name'] = 'Antes era';
$lang['log_unbanned'] = 'Desbanido';
$lang['log_customization'] = 'Obtenha uma personalização';
$lang['log_change_race'] = 'Obter uma Mudança de Raça';
$lang['log_change_faction'] = 'Obter uma Mudança de Facção';
$lang['log_banned'] = 'Foi banido';
$lang['log_gm_assigned'] = 'Rank GM recebido';
$lang['log_gm_removed'] = 'O Rank GM foi removido';

/*CMS Lang*/
$lang['cms_version_currently'] = 'Esta versão está atualmente em execução';
$lang['cms_warning_update'] = 'Quando o cms é atualizado a configuração pode ser restaurada para o padrão, dependendo das alterações feitas a cada versão.';
$lang['cms_php_version'] = 'Versão PHP';
$lang['cms_allow_fopen'] = 'allow_url_fopen';
$lang['cms_allow_include'] = 'allow_url_include';
$lang['cms_loaded_modules'] = 'Módulos carregados';
$lang['cms_loaded_extensions'] = 'Loaded Extensions';
